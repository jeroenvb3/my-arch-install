#! /bin/bash

# If any line fails, exit script.
trap 'previous_command=$BASH_COMMAND' DEBUG
trap "echo $previous_command" EXIT
set -e

INSTALL_PATH=$(pwd)

cd ~

sudo pacman -Syu

# Install yay.
mkdir -p aur_programs
cd aur_programs
if [ ! -x "$(command -v yay)" ]; then
    if [ -d "yay" ]; then
        sudo rm -r yay
    fi
    git clone https://aur.archlinux.org/yay.git
    cd yay
    makepkg -si
    cd ..
fi

# Install packages that pacman can install.
yay -S --needed - < $INSTALL_PATH/packages.txt

# Install vim files
 # Clone into ~/Documents/vim
 mkdir -p ~/Documents
 cd ~/Documents
 if [ ! -d "my-vim-files" ]; then
     vim_git="https://gitlab.com/jeroenvb3/my-vim-files.git"
     git clone $vim_git
 fi
 # Symlink it with ~/.vimrc and ~/.vim
 ln -sf ~/Documents/vim/.vim ~/.vim
 ln -sf ~/Documents/vim/.vimrc ~/.vimrc
# Install zsh files
 cd ~/Documents
 # Clone into ~/Documents/zsh
 if [ ! -d "zsh" ]; then
     zsh_git="https://gitlab.com/jeroenvb3/zsh.git"
     git clone $zsh_git
 fi
 # Symlink it with ~/.zshrc and ~/.oh-my-zsh
 ln -sf ~/Documents/zsh/.zsh_aliases ~/.zsh_aliases
 ln -sf ~/Documents/zsh/.zshrc ~/.zshrc
 ln -sf ~/Documents/zsh/.oh-my-zsh ~/.oh-my-zsh

# Set git username etc.
git config --global user.name "Jeroen van Bennekum"
git config --global user.email "jeroen_-@live.nl"

# TODO Install dwm
mkdir -p /usr/src
cd /usr/src
sudo chown -R jeroen .
if [ ! -d "dwm" ]; then
    dwm_git="https://gitlab.com/jeroenvb3/dwm.git"
    git clone $dwm_git
fi 
cd dwm
sudo make clean install

# TODO Connect DWM to lightdm
sudo systemctl enable lightdm
# TODO Install st
# TODO Install dmenu
# TODO Set zsh as default terminal

# Manual TODO's
# TODO Add ssh key to gitlab.
# TODO visudo ...
